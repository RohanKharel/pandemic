﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    int LvlNum;

    public Slider mainslider;

    private void Start()
    {
        mainslider.wholeNumbers = true;
        mainslider.onValueChanged.AddListener(HandleValueChanged);
    }

    private void HandleValueChanged(float value)
    {
        LvlNum = Convert.ToInt32(mainslider.value);
    }

    public void PlayLevel ()
    {
        SceneManager.LoadScene(LvlNum);
    }
}
